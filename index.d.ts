type ISO8601Date = string
type TDate = ISO8601Date

export interface ICollection<T extends {}> {
  object: "collection"
  url: URLString
  pages: {
    next_url: URLString | null
    previous_url: URLString | null
    per_page: number
  }
  total_count: number
  data_updated_at: TDate
  data: T[]
}

export interface IResource<TData> extends IResourceNoId<TData> {
  id: number
}

export interface IResourceNoId<TData> {
  object: string
  url: URLString
  data_updated_at: TDate
  data: TData
}

export interface IMeaning {
  meaning: string
  primary: boolean
  accepted_answer: boolean
}

export interface ISubjectDataCommon {
  created_at: TDate
  level: number
  slug: string
  hidden_at: TDate | null
  meanings: IMeaning[]
  document_url: string
}

export interface IRasterImageMetadata {
  color: ColorHexString
  dimensions: SizeString
  style_name: string
}

export interface ISvgImageMetadata {
  inline_styles: boolean
}

export interface IRadicalDataCharacterImage {
  url: URLString
  metadata: IRasterImageMetadata | ISvgImageMetadata
  content_type: string
}

export interface IRadicalData extends ISubjectDataCommon {
  characters: string | null
  character_images: IRadicalDataCharacterImage[]
  amalgamation_subject_ids: number[]
}

export interface IRadical extends IResource<IRadicalData> {
  object: "radical"
}

export interface IReading {
  reading: string
  primary: boolean
  accepted_answer: boolean
}

export interface IKanjiReading extends IReading {
  type: "onyomi" | "kunyomi" | "nanori"
}

export interface IKanjiData extends ISubjectDataCommon {
  characters: string
  readings: IKanjiReading[]
  component_subject_ids: number[]
  amalgamation_subject_ids: number[]
}

export interface IKanji extends IResource<IKanjiData> {
  object: "kanji"
}

export interface IVocabularyData extends ISubjectDataCommon {
  characters: string
  readings: IReading[]
  parts_of_speech: string[]
  component_subject_ids: number[]
}

export interface IVocabulary extends IResource<IVocabularyData> {
  object: "vocabulary"
}

export type Subject = IRadical | IKanji | IVocabulary

export interface IUserData {
  username: string
  level: number
  max_level_granted_by_subscription: number
  profile_url: string
  subscribed: boolean
  current_vacation_started_at: TDate | null
}

export interface IUser extends IResourceNoId<IUserData> {
  object: "user"
}

/**
 * SRS completion and date stats.
 * The user only has access to assignments they have unlocked.
 */
export interface IAssignment extends IResource<IAssignmentData> {
  object: "assignment"
}

export interface IAssignmentData {
  created_at: TDate
  subject_id: number
  subject_type: string
  level: number
  srs_stage: number
  srs_stage_name: string
  unlocked_at: TDate | null
  started_at: TDate | null
  passed_at: TDate | null
  burned_at: TDate | null
  resurrected_at: TDate | null
  available_at: TDate | null
  passed: boolean
  resurrected: boolean
  hidden: boolean
}

export interface IReviewStatistic extends IResource<IReviewStatisticData> {
  object: "review_statistic"
}

export interface IReviewStatisticData {
  created_at: TDate
  subject_id: number
  subject_type: SubjectType
  meaning_correct: number
  meaning_incorrect: number
  meaning_max_streak: number
  meaning_current_streak: number
  reading_correct: number
  reading_incorrect: number
  reading_max_streak: number
  reading_current_streak: number
  percentage_correct: number
  hidden: boolean
}

/**
 * When the user has added a note or synonym, a `study_material` resource for the subject is created.
 */
export interface IStudyMaterial extends IResource<IStudyMaterialData> {
  object: "study_material"
}

export interface IStudyMaterialData {
  created_at: TDate
  subject_id: number
  subject_type: SubjectType
  meaning_note: string | null
  reading_note: string | null
  meaning_synonyms: string[]
  hidden: boolean
}

export interface ISummary extends IResourceNoId<ISummaryData> {
  object: "report"
}

/**
 * `reviews` and `lessons` contain a collection of objects with keys `available_at` and `subject_ids`. `subject_ids` are the subjects appearing on `available_at`. We return for each hour up to 24 hours for `reviews`. `lessons` only contains one object.
 */
export interface ISummaryData {
  // Contains one item
  lessons: { available_at: TDate; subject_ids: number[] }
  // Contains 25 items, one for each hour
  reviews: { available_at: TDate; subject_ids: number[] }
  next_reviews_at: TDate | null
}

export interface IReview extends IResource<IReviewData> {}

export interface IReviewData {
  created_at: TDate
  assignment_id: number
  subject_id: number
  starting_srs_stage: number
  starting_srs_stage_name: string
  ending_srs_stage: number
  ending_srs_stage_name: string
  incorrect_meaning_answers: number
  incorrect_reading_answers: number
}

/**
 * Recorded when the user levels up.
 */
export interface ILevelProgression extends IResource<ILevelProgressionData> {
  object: "level_progression"
}

export interface ILevelProgressionData {
  created_at: TDate
  level: number
  unlocked_at: TDate | null
  /** gets timestamped when the user finishes the first lesson for a subject belonging to the level */
  started_at: TDate | null
  /**
   * gets timestamp once the user passes (reach Guru [5] srs stage) 90% of the kanji subject for that level.
   */
  passed_at: TDate | null
  /**
   * gets timestamp when the user reaches srs_stage 5 on all the subjects associated to the level.
   */
  completed_at: TDate | null
  /**
   * gets timestamp for events such as a user reset
   */
  abandoned_at: TDate | null
}

export interface IReset extends IResource<IResetData> {}

export interface IResetData {
  created_at: TDate
  original_level: number
  target_level: number
  confirmed_at: TDate
}

type SubjectType = "radical" | "kanji" | "vocabulary"
/** #ABCDEF */
type ColorHexString = string
/** ${width}x${height} */
type SizeString = string
type URLString = string
