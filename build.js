var FS = require("fs")

var content = FS.readFileSync(`${__dirname}/index.d.ts`, { encoding: "utf8" })

content = content.replace(/(type TDate = )(ISO8601Date)$/m, "$1Date")

FS.writeFileSync(`${__dirname}/jsdate.d.ts`, content, { encoding: "utf8" })
