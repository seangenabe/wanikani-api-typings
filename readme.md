# WaniKani typings

TypeScript typings for the WaniKani API.

## Flavors

* index.d.ts - Dates are based on `string`.
* jsdate.d.ts - Dates are base on `Date`.

## Changelog

* 1.0.0-alpha+2.alpha.20180605 - [v2 alpha](https://community.wanikani.com/t/api-v2-alpha-documentation/18987)
